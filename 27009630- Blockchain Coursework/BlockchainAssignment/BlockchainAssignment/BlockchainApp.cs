﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Diagnostics;

namespace BlockchainAssignment
{
    public partial class BlockchainApp : Form
    {
        // Global blockchain object
        private Blockchain blockchain;
        public static bool threading;
        public static bool difficultyCheck;


        // Default App Constructor
        public BlockchainApp()
        {
            // Initialise UI Components
            InitializeComponent();
            // Create a new blockchain 
            blockchain = new Blockchain();
            // Update UI with an initalisation message
            UpdateText("New blockchain initialised!");
        }

        public static bool getThreadingOption() {
            return threading;
            
            
        }
        public static bool getDifficultyOption() {
            return difficultyCheck;
        }
        /* PRINTING */
        // Helper method to update the UI with a provided message
        private void UpdateText(String text)
        {
            output.Text = text;
        }

        // Print entire blockchain to UI
        private void ReadAll_Click(object sender, EventArgs e)
        {
            UpdateText(blockchain.ToString());
        }

        // Print Block N (based on user input)
        private void PrintBlock_Click(object sender, EventArgs e)
        {
            if (Int32.TryParse(blockNo.Text, out int index))
                UpdateText(blockchain.GetBlockAsString(index));
            else
                UpdateText("Invalid Block No.");
        }

        // Print pending transactions from the transaction pool to the UI
        private void PrintPendingTransactions_Click(object sender, EventArgs e)
        {
            if (radioButton1.Checked == true && blockchain.transactionPool.Count != 0) { //if default is checked and transaction pool isnt empty
                blockchain.transactionPool.Sort((y, x) => x.timestamp.CompareTo(y.timestamp)); // display default youngest to oldest
                UpdateText(String.Join("\n", blockchain.transactionPool)); 

            }
            else if (Greedy.Checked == true && blockchain.transactionPool.Count != 0) //if greedy checked and transaction pool isnt empty
            {
                blockchain.transactionPool.Sort((y, x) => x.fee.CompareTo(y.fee)); //sort fees high to low and display
                
                UpdateText(String.Join("\n", blockchain.transactionPool));
            }
            else if (Althrustic.Checked == true && blockchain.transactionPool.Count != 0) //if Alturistic is checked and transaction pool isnt empty
            {
      
                blockchain.transactionPool.Sort((x, y) => x.timestamp.CompareTo(y.timestamp)); //sort by oldest blocks and display
                UpdateText(String.Join("\n", blockchain.transactionPool));
            }
            else if (Unpredictable.Checked == true && blockchain.transactionPool.Count != 0) //if unpredictable check and transaction pool isnt empty
            {
                Random rnd = new Random(); // initialise random
                int i = blockchain.transactionPool.Count;
                while (i > 1) //go through the transactin pool backwards
                {
                    i--;
                    int k = rnd.Next(i + 1); //randomise
                    Transaction value = blockchain.transactionPool[k];
                    blockchain.transactionPool[k] = blockchain.transactionPool[i];
                    blockchain.transactionPool[i] = value;

                }

                UpdateText(String.Join("\n", blockchain.transactionPool)); //display
            }
            else if (Address_based.Checked == true && blockchain.transactionPool.Count != 0) //if adress checked and transaction pool isnt empty
            {
                List<Transaction> tempList = new List<Transaction>();
                foreach (Transaction transaction in blockchain.transactionPool) //for every transaction in transaction pool
                {
                    // public List<Transaction> transactionPool = new List<Transaction>();
                    String address = Personal_address_textbox.Text; //grab the adress inputted in the textbox

                    if (address.Equals(transaction.senderAddress)) //if its equal to any adresses of the transactions
                    {
                        tempList.Add(transaction); //add it to the temp list
                        
                    }

                }

                UpdateText(String.Join("\n", tempList)); //display the temp list
            }
        }

        /* WALLETS */
        // Generate a new Wallet and fill the public and private key fields of the UI
        private void GenerateWallet_Click(object sender, EventArgs e)
        {
            Wallet.Wallet myNewWallet = new Wallet.Wallet(out string privKey);

            publicKey.Text = myNewWallet.publicID;
            privateKey.Text = privKey;
        }

        // Validate the keys loaded in the UI by comparing their mathematical relationship
        private void ValidateKeys_Click(object sender, EventArgs e)
        {
            if (Wallet.Wallet.ValidatePrivateKey(privateKey.Text, publicKey.Text))
                UpdateText("Keys are valid");
            else
                UpdateText("Keys are invalid");
        }

        // Check the balance of current user
        private void CheckBalance_Click(object sender, EventArgs e)
        {
            UpdateText(blockchain.GetBalance(publicKey.Text).ToString() + " Assignment Coin");
        }


        /* TRANSACTION MANAGEMENT */
        // Create a new pending transaction and add it to the transaction pool
        private void CreateTransaction_Click(object sender, EventArgs e)
        {
            Transaction transaction = new Transaction(publicKey.Text, reciever.Text, Double.Parse(amount.Text), Double.Parse(fee.Text), privateKey.Text);
            /* TODO: Validate transaction */
            blockchain.transactionPool.Add(transaction);
            UpdateText(transaction.ToString());
        }

        /* BLOCK MANAGEMENT */
        // Conduct Proof-of-work in order to mine transactions from the pool and submit a new block to the Blockchain
        private void NewBlock_Click(object sender, EventArgs e)
            
        {

            
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
           

            // Retrieve pending transactions to be added to the newly generated Block
            List<Transaction> transactions = blockchain.GetPendingTransactions();

            // Create and append the new block - requires a reference to the previous block, a set of transactions and the miners public address (For the reward to be issued)
            Block newBlock = new Block(blockchain.GetLastBlock(), transactions, publicKey.Text);
            blockchain.blocks.Add(newBlock);
                
            
            stopwatch.Stop();
            UpdateText(blockchain.ToString());
           /* if (threading == false)
            {
                Console.WriteLine(amount + " Coins, Elapsed Time is {0} ms", (stopwatch.ElapsedMilliseconds * 1.5));
            }
            else {
                Console.WriteLine(amount + " Coins, Elapsed Time is {0} ms", stopwatch.ElapsedMilliseconds);
            }*/
        }


        /* BLOCKCHAIN VALIDATION */
        // Validate the integrity of the state of the Blockchain
        private void Validate_Click(object sender, EventArgs e)
        {
            // CASE: Genesis Block - Check only hash as no transactions are currently present
            if(blockchain.blocks.Count == 1)
            {
                if (!Blockchain.ValidateHash(blockchain.blocks[0])) // Recompute Hash to check validity
                    UpdateText("Blockchain is invalid");
                else
                    UpdateText("Blockchain is valid");
                return;
            }

            for (int i=1; i<blockchain.blocks.Count-1; i++)
            {
                if(
                    blockchain.blocks[i].prevHash != blockchain.blocks[i - 1].hash || // Check hash "chain"
                    !Blockchain.ValidateHash(blockchain.blocks[i]) ||  // Check each blocks hash
                    !Blockchain.ValidateMerkleRoot(blockchain.blocks[i]) // Check transaction integrity using Merkle Root
                )
                {
                    UpdateText("Blockchain is invalid");
                    return;
                }
            }
            UpdateText("Blockchain is valid");
        }

        private void validationLabel_Click(object sender, EventArgs e)
        {

        }

        private void amount_TextChanged(object sender, EventArgs e)
        {

        }

        private void blocksLabel_Click(object sender, EventArgs e)
        {

        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void blockNo_TextChanged(object sender, EventArgs e)
        {

        }

        private void Greedy_CheckedChanged(object sender, EventArgs e)
        {
            
        }

       

       

        private void BlockchainApp_Load(object sender, EventArgs e)
        {

        }

        private void DifficultyBox_CheckedChanged(object sender, EventArgs e)
        {
            if (DifficultyBox.Checked == true)
            {
                difficultyCheck = true;

            }
            else difficultyCheck = false;
        }

        private void Threading_CheckedChanged(object sender, EventArgs e)
        {

            if (Threading.Checked == true)
            {
                threading = true;

            }
            else threading = false;
        }

        private void Address_based_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void feeLabel_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void currentWalletLabel_Click(object sender, EventArgs e)
        {

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void transactionLabel_Click(object sender, EventArgs e)
        {

        }

        private void groupBox3_Enter(object sender, EventArgs e)
        {

        }

        private void fee_TextChanged(object sender, EventArgs e)
        {

        }

        private void amountLabel_Click(object sender, EventArgs e)
        {

        }

        private void groupBox6_Enter(object sender, EventArgs e)
        {

        }

        private void groupBox7_Enter(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }
    }
}