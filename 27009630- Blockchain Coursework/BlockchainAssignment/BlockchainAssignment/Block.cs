﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Diagnostics;
using System.Threading.Tasks;



namespace BlockchainAssignment
{
    class Block
    {
        /* Block Variables */
        public DateTime timestamp; // Time of creation
        public long blockTime;

        public int blockDifficulty; 



        private int index; // Position of the block in the sequence of blocks
    

        public String prevHash, // A reference pointer to the previous block
            hash, // The current blocks "identity"
            merkleRoot,  // The merkle root of all transactions in the block
            minerAddress; // Public Key (Wallet Address) of the Miner

        public List<Transaction> transactionList; // List of transactions in this block

       
        // Proof-of-work
        public long nonce; // Number used once for Proof-of-Work and mining
        private long evenNonce = 0;
        private long oddNonce = 1;
        
        private string evenHash = "", oddHash = "";
        private bool threadOneFinished = false, threadTwoFinished = false;



        // Rewards
        public double reward; // Simple fixed reward established by "Coinbase"

        /* Genesis block constructor */
        public Block()
        {
            timestamp = DateTime.Now;
            index = 0;
            transactionList = new List<Transaction>();
            blockDifficulty = Blockchain.difficulty;


            if (BlockchainApp.getThreadingOption() == false)
            {

                hash = Mine();


            }
            if (BlockchainApp.getThreadingOption() == true)
            {

                hash = hashThread();

            }

        }

        /* New Block constructor */
        public Block(Block lastBlock, List<Transaction> transactions, String minerAddress)
        {
            timestamp = DateTime.Now;

            index = lastBlock.index + 1;
            prevHash = lastBlock.hash;
            blockDifficulty = Blockchain.difficulty;
            this.minerAddress = minerAddress; // The wallet to be credited the reward for the mining effort
            reward = 1.0; // Assign a simple fixed value reward
            transactions.Add(createRewardTransaction(transactions)); // Create and append the reward transaction
            transactionList = new List<Transaction>(transactions); // Assign provided transactions to the block
            

            merkleRoot = MerkleRoot(transactionList); // Calculate the merkle root of the blocks transactions
            
            if (BlockchainApp.getThreadingOption() == false)

            {
               
                if ((BlockchainApp.getDifficultyOption() == true) && (Blockchain.blockTimes.Count % 4 == 0) ) {

                    AdjustDifficulty(); //every four block check for adjusting difficulty
                    
                       }
                hash = Mine();


            }
            if (BlockchainApp.getThreadingOption() == true) {
                
                if ((BlockchainApp.getDifficultyOption() == true) && (Blockchain.blockTimes.Count % 4 == 0) )
                {
                 
                    AdjustDifficulty();  //every four block check for adjusting difficulty

                }

                hash = hashThread();
            
            } // Conduct PoW to create a hash which meets the given difficulty requirement
        }

     

        public void AdjustDifficulty() { 
              
                
            double average = Blockchain.blockTimes.Average(); // mean average
            long comparitor = Blockchain.blockTimes.First(); // first time recorded in the block times
            
            long lowerBound = (long)(comparitor * 0.9); // lowerbound
            long higherBound = (long)(comparitor * 1.3); //higherbound
            if (average < lowerBound) { //if average is less than lowerbound
                Blockchain.difficulty += 1; //increase difficulty due it being too easy
                blockDifficulty += 1;
        
                Console.WriteLine("Average Time: " + average);
                Console.WriteLine("First Element: " + comparitor);
                Console.WriteLine("Too Easy so increasing difficulty");
                Console.WriteLine("New Difficulty: " + Blockchain.difficulty);
                Blockchain.blockTimes.Clear(); //clear the list as new difficulty is present
            }
            if ((average > higherBound) && (Blockchain.difficulty != 1) ) { //if average is more than higherbound and also diffculty isnt one
                Blockchain.difficulty -= 1;  //decrease difficulty due it being too easy
                blockDifficulty -= 1;
          
                Console.WriteLine("Average Time: " + average);
                Console.WriteLine("First Element: " + comparitor);
                Console.WriteLine("Too Hard so decreasing difficulty");
                Console.WriteLine("New Difficulty: " + Blockchain.difficulty);
                Blockchain.blockTimes.Clear(); //clear the list as new difficulty is present

            }
            
        
        }
        public int getIndex()
        {
            return index;
        }

        /* Hashes the entire Block object */

        public String CreateHash()
        {
            String hash = String.Empty;
            SHA256 hasher = SHA256Managed.Create();

            /* Concatenate all of the blocks properties including nonce as to generate a new hash on each call */
            String input = timestamp.ToString() + index + prevHash + nonce + merkleRoot;

            /* Apply the hash function to the block as represented by the string "input" */
            Byte[] hashByte = hasher.ComputeHash(Encoding.UTF8.GetBytes(input));

            /* Reformat to a string */
            foreach (byte x in hashByte)
                hash += String.Format("{0:x2}", x);
            
            return hash;
        }
        public String CreateHash(long nonce)
        {
            String hash = String.Empty;
            SHA256 hasher = SHA256Managed.Create();

            /* Concatenate all of the blocks properties including nonce as to generate a new hash on each call */
            String input = timestamp.ToString() + index + prevHash + nonce + merkleRoot;

            /* Apply the hash function to the block as represented by the string "input" */
            Byte[] hashByte = hasher.ComputeHash(Encoding.UTF8.GetBytes(input));

            /* Reformat to a string */
            foreach (byte x in hashByte)
                hash += String.Format("{0:x2}", x);

            return hash;
        }

        // Create a Hash which satisfies the difficulty level required for PoW
        public String Mine()

        {

            Stopwatch sw = new Stopwatch();
            sw.Start();
            string hash = "";          
            string diffString = new string('0', Blockchain.difficulty);
            while (hash.StartsWith(diffString) == false)
            {
                hash = this.CreateHash(nonce);
                this.nonce++;
            }
            this.nonce--;
            Console.WriteLine("Time: " + sw.ElapsedMilliseconds);
            blockTime = sw.ElapsedMilliseconds;
            Blockchain.blockTimes.Add(blockTime);
            return hash;
        }
        public string hashThread()

        {

            Stopwatch sw = new Stopwatch();
            sw.Start();
            string re = new string('0', Blockchain.difficulty);
            Thread th1 = new Thread(mineEven); //initialise threads to do the even and odd mining
            Thread th2 = new Thread(mineOdd);

            th1.Start();    //start and join threads
            th2.Start();
            th1.Join();
            th2.Join();

            while (th1.IsAlive == true || th2.IsAlive == true) { Thread.Sleep(1); } //while the threads are alive

            if (evenHash.StartsWith(re) == true) //if solution found with even nonce
            {
                nonce = evenNonce;
                sw.Stop();
                Console.WriteLine("Time: " + sw.ElapsedMilliseconds);
                blockTime = sw.ElapsedMilliseconds;
                Blockchain.blockTimes.Add(blockTime); //add time taken to list
                return evenHash; //return hash
                 
            }
            else
            {
                nonce = oddNonce; //if solution found with odd nonce
                Console.WriteLine("Time: " + sw.ElapsedMilliseconds);

                sw.Stop();
                blockTime = sw.ElapsedMilliseconds;
                Blockchain.blockTimes.Add(blockTime);//add time taken to list
                return oddHash;  //return hash

            }
     

        }

        public void mineEven() //mining for even n once
        {    
            Boolean check = false;
            String temporary_hash;
            string diffString = new string('0', Blockchain.difficulty);

            while (check == false)
            {
                temporary_hash = CreateHash(evenNonce);
                if (temporary_hash.StartsWith(diffString) == true) //if even hash found soluttion
                {
                    check = true;
                    evenHash = temporary_hash; //store hash

                    threadOneFinished = true; //thread is finished 

                    return;
                }
                else if (oddHash.StartsWith(diffString) == true) //if odd hash found solution
                {
                
                    Thread.Sleep(1);
              
                    return;
                }
                else
                {
                    check = false;
                    evenNonce += 2; //increment even nonce by 2, eg 0,2,4
                }
            }
            return;
        }

        public void mineOdd()
        {
            Boolean check = false;
            String temporary_hash;
            string diffString = new string('0', Blockchain.difficulty);
            while (check == false)
            {
                temporary_hash = CreateHash(oddNonce);
                if (temporary_hash.StartsWith(diffString) == true) //if odd hash found soluttion
                {
                    check = true;
                    oddHash = temporary_hash;  //store hash

                    threadTwoFinished = true; //thread is finished 

                    return;
                }
                else if (evenHash.StartsWith(diffString) == true) //if even hash found solution
                {
                  
                    Thread.Sleep(1);
                    return;
                }
                else
                {
                    check = false;
                    oddNonce += 2; //increment even nonce by 2, eg 1,3,5
                } 
            }
            return;
        }



        // Merkle Root Algorithm - Encodes transactions within a block into a single hash
        public static String MerkleRoot(List<Transaction> transactionList)
        {
            List<String> hashes = transactionList.Select(t => t.hash).ToList(); // Get a list of transaction hashes for "combining"
            
            // Handle Blocks with...
            if (hashes.Count == 0) // No transactions
            {
                return String.Empty;
            }
            if (hashes.Count == 1) // One transaction - hash with "self"
            {
                return HashCode.HashTools.combineHash(hashes[0], hashes[0]);
            }
            while (hashes.Count != 1) // Multiple transactions - Repeat until tree has been traversed
            {
                List<String> merkleLeaves = new List<String>(); // Keep track of current "level" of the tree

                for (int i=0; i<hashes.Count; i+=2) // Step over neighbouring pair combining each
                {
                    if (i == hashes.Count - 1)
                    {
                        merkleLeaves.Add(HashCode.HashTools.combineHash(hashes[i], hashes[i])); // Handle an odd number of leaves
                    }
                    else
                    {
                        merkleLeaves.Add(HashCode.HashTools.combineHash(hashes[i], hashes[i + 1])); // Hash neighbours leaves
                    }
                }
                hashes = merkleLeaves; // Update the working "layer"
            }
            return hashes[0]; // Return the root node
        }

        // Create reward for incentivising the mining of block
        public Transaction createRewardTransaction(List<Transaction> transactions)
        {
            double fees = transactions.Aggregate(0.0, (acc, t) => acc + t.fee); // Sum all transaction fees
            return new Transaction("Mine Rewards", minerAddress, (reward + fees), 0, ""); // Issue reward as a transaction in the new block
        }

        /* Concatenate all properties to output to the UI */
        public override string ToString()
        {
            return "[BLOCK START]"
                + "\nIndex: " + index
                + "\tTimestamp: " + timestamp
                + "\nPrevious Hash: " + prevHash
                + "\n-- PoW --"
                + "\nDifficulty Level: " + blockDifficulty
                + "\nNonce: " + nonce
                + "\nHash: " + hash
                + "\n-- Rewards --"
                + "\nReward: " + reward
                + "\nMiners Address: " + minerAddress
                + "\n-- " + transactionList.Count + " Transactions --"
                +"\nMerkle Root: " + merkleRoot
                + "\n" + String.Join("\n", transactionList)
                + "\n[BLOCK END]";
        }
    }
}
